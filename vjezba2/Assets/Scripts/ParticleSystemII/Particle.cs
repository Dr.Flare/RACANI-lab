using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ParticleSystemII
{
    public class Particle : MonoBehaviour
    {
        private ParticleSystem ps;
        
        private Quaternion orientation;
        private Vector3 _position;
        private Vector3 _velocity;
        private Vector3 _acceleration;

        private float remainingLifetime;

        private float FPS;
        private uint currentFrame = 0;

        public Particle()
        {
        }

        public Particle(Vector3 position, Vector3 velocity, Vector3 acceleration)
        {
            _position = position;
            _velocity = velocity;
            _acceleration = acceleration;

            transform.rotation = orientation;
        }

        public float lifetime
        {
            get => remainingLifetime;
            set => remainingLifetime = value;
        }
        public Vector3 position
        {
            get => _position;
            set => _position = value;
        }
        public Vector3 velocity
        {
            get => _velocity;
            set => _velocity = value;
        }
        public Vector3 acceleration
        {
            get => _acceleration;
            set => _acceleration = value;
        }

        private void Start()
        {
            FPS = 1f / Time.deltaTime;
            _position = transform.position;
            
            ps = FindObjectOfType<ParticleSystem>();
        }

        private void Update()
        {
            _velocity += _acceleration * Time.deltaTime;
            _position += _velocity * Time.deltaTime;
            lifetime -= Time.deltaTime;

            float t = 1;
            if (lifetime < 3)
            {
                t = lifetime / 3;
            }

            Color currentColor = Color.Lerp(Color.red, Color.blue, t);
            GetComponent<Renderer>().material.color = currentColor;
            
            transform.position = _position;
            transform.LookAt(Camera.main.transform);
            transform.Rotate(Vector3.right, 90);
            print("position = " + _position 
                                + "\nvelocity = " + _velocity 
                                + "\nacceleration = " + _acceleration 
                                + "\nremaining lifetime = " + lifetime 
                                + "\norientation = " + transform.rotation.eulerAngles
                                + "\ndeltaTime = " + Time.deltaTime);

            if (lifetime <= 0)
            {
                ps.particleDied();
                Destroy(gameObject);
            }
        }
    }
}