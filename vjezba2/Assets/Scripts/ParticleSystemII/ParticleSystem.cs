﻿using System;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ParticleSystemII
{
    public class ParticleSystem : MonoBehaviour
    {
        public Vector3 position;
        public Quaternion orientation;

        private const uint maxParticles = 100;
        private uint currentParticles = 0;

        public float particleLifetime = 200;

        public float spawnInterval = 2f;
        private float FPS;
        private uint spawnCounter = 0;

        public GameObject particleObject;
        
        public Vector3 particlePosition = Vector3.zero;
        public Vector3 particleVelocity = new (0f, 0.001f, 0f);
        public Vector3 particleAccel = new (0f, 0.0005f, 0f);
        
        public Vector3 particlePositionVariance = Vector3.zero;
        public Vector3 particleVelocityVariance = Vector3.zero;
        public Vector3 particleAccelVariance = Vector3.zero;
        
        public uint particleLifetimeVariance = 100;
        public uint spawnIntervalVariance = 100;
        private float nextSpawnTimeVariance = 0;

        private void Start()
        {
            FPS = 1f / Time.deltaTime;
            Random.InitState(Guid.NewGuid().GetHashCode());
        }

        private void Update()
        {
            if (currentParticles < maxParticles && (spawnCounter + nextSpawnTimeVariance) / FPS > spawnInterval)
            {
                spawnCounter = 0;
                float x, y, z; // temp


                x = Random.Range(particlePosition.x - (particlePositionVariance.x / 2), 
                    particlePosition.x + (particlePositionVariance.x / 2));
                y = Random.Range(particlePosition.y - (particlePositionVariance.y / 2), 
                    particlePosition.y + (particlePositionVariance.y / 2));
                z = Random.Range(particlePosition.z - (particlePositionVariance.z / 2), 
                    particlePosition.z + (particlePositionVariance.z / 2));

                var newPos = new Vector3(x, y, z);

                var obj = Instantiate(particleObject, newPos + position, orientation);
                Particle particleScript = obj.GetComponent<Particle>();

                particleScript.position = newPos;
                particleScript.GetComponent<Renderer>().material.color = Color.blue;

                print("generirana vrijednost inicijalne pozicije: " + particleScript.position);
                
                x = Random.Range(particleVelocity.x - (particleVelocityVariance.x / 2), 
                    particleVelocity.x + (particleVelocityVariance.x / 2));
                y = Random.Range(particleVelocity.y - (particleVelocityVariance.y / 2), 
                    particleVelocity.y + (particleVelocityVariance.y / 2));
                z = Random.Range(particleVelocity.z - (particleVelocityVariance.z / 2), 
                    particleVelocity.z + (particleVelocityVariance.z / 2));

                particleScript.velocity = new Vector3(x, y, z);                
                
                x = Random.Range(particleAccel.x - (particleAccelVariance.x / 2), 
                    particleAccel.x + (particleAccelVariance.x / 2));
                y = Random.Range(particleAccel.y - (particleAccelVariance.y / 2), 
                    particleAccel.y + (particleAccelVariance.y / 2));
                z = Random.Range(particleAccel.z - (particleAccelVariance.z / 2), 
                    particleAccel.z + (particleAccelVariance.z / 2));
                
                particleScript.acceleration = new Vector3(x, y, z);

                particleScript.lifetime = Random.Range(particleLifetime - (particleLifetimeVariance / 2),
                                                              particleLifetime + (particleLifetimeVariance / 2));
                
                print("lifetime = " + particleScript.lifetime + " iz min " + (particleLifetime - (particleLifetimeVariance / 2)) + " i max " +
                      (particleLifetime + (particleLifetimeVariance / 2)) + ", cast u uint = " + x);


                currentParticles++;
                
                nextSpawnTimeVariance = Random.Range(spawnInterval - (spawnIntervalVariance / 2f), 
                    spawnInterval + (spawnIntervalVariance / 2f));
            }
            else
            {
                spawnCounter++;
            }
        }

        public void particleDied()
        {
            currentParticles--;
        }
    }
}