#include "TransformControls.h"
#include<iostream>

namespace transform_controls{
	static GLFWwindow *window;
	static Transform *transform;

	GLFWkeyfun previousCallback = nullptr;

	double centerX, centerY, lastKnownX, lastKnownY;

	static void cursorPosCallback(GLFWwindow *win, double xpos, double ypos) {
		float dx = lastKnownX - xpos;
		float dy = lastKnownY - ypos;

//		glfwSetCursorPos(win, centerX, centerY);

		if (glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
			transform->moveCursorX(dx);
			transform->moveCursorY(dy);
		}

		lastKnownX = xpos;
		lastKnownY = ypos;
	}

	static void scrollCallback(GLFWwindow *win, double xoff, double yoff) {

		glm::vec3 amount = -transform->getForward();
		amount *= yoff;

		transform->move(amount);
	}

	static void keyPressCallback(GLFWwindow *win, int key, int scancode, int action, int mods) {
		glm::vec3 u = transform->getUp();
		glm::vec3 d = -u;
		glm::vec3 l = transform->getRight();
		glm::vec3 r = -l;
		glm::vec3 b = transform->getForward();
		glm::vec3 f = -b;

		if (action == GLFW_RELEASE) {
			return;
		}

		switch (key) {
			case GLFW_KEY_Q: {
				transform->move(u);
				break;
			}
			case GLFW_KEY_W: {
				transform->move(f);
				break;}
			case GLFW_KEY_E: {
				transform->move(d);
				break;}
			case GLFW_KEY_A: {
				transform->move(l);
				break;}
			case GLFW_KEY_S: {
				transform->move(b);
				break;}
			case GLFW_KEY_D: {
				transform->move(r);
				break;}
		}
	}

	static void combineCallbacks(GLFWwindow *win, int key, int scancode, int action, int mods) {
		if (previousCallback != nullptr) {
			previousCallback(win, key, scancode, action, mods);
		}
		keyPressCallback(win, key, scancode, action, mods);
	}

	void registerCallback() {
		glfwSetCursorPosCallback(window, cursorPosCallback);
		previousCallback = glfwSetKeyCallback(window, nullptr);
		glfwSetKeyCallback(window, combineCallbacks);
		glfwSetScrollCallback(window, scrollCallback);
	}

	void registerTransform (GLFWwindow *w, Transform *t) {
		window = w;
		transform = t;
		lastKnownX = centerX = M_WIDTH / 2.0;
		lastKnownY = centerY = M_HEIGHT / 2.0;
		registerCallback();
	}
};