// System Headers
#include <glad/glad.h>
#include <GL/freeglut.h>
#include <GLFW/glfw3.h>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define STB_IMAGE_IMPLEMENTATION

#include "stb_image.h"
#include "Shader.h"

#ifndef __MOUSECONTROLS_H__
#define __MOUSECONTROLS_H__

#include "TransformControls.h"

#endif

#ifndef __MAIN_H__
#define __MAIN_H__

#include "main.h"

#endif

// Standard Headers
#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

int mWidth = M_WIDTH;
int mHeight = M_HEIGHT;


const float CURVE_PRECISION = 0.0001;
float ANIMATION_SPEED = 20;
const glm::mat4 aspectRatioFix = glm::scale(glm::vec3(1.0 * mHeight / mWidth, 1.0, 1.0));

void framebuffer_size_callback(GLFWwindow *window, int Width, int Height) {
	mHeight = Height;
	mWidth = Width;
	glfwSetWindowSize(window, mWidth, mHeight);
}

glm::mat4 identityMatrix() {
	return glm::mat4(1.0f);
}

struct Point3D {
	glm::vec4 coords;

	Point3D(const glm::vec3 &coords) : coords(coords, 1) {}

	Point3D(const glm::vec4 &coords) : coords(coords) {}

	Point3D(const float x, const float y, const float z) : coords(glm::vec4(x, y, z, 1)) {}

	Point3D(const float x, const float y, const float z, const float h) : coords(glm::vec4(x, y, z, h)) {}

	Point3D() : coords(glm::vec4(0, 0, 0, 1)) {}

	Point3D(const Point3D &other) {
		coords = other.coords;
	}

	float x() {
		return coords.x;
	}

	float y() {
		return coords.y;
	}

	float z() {
		return coords.z;
	}

	float &operator[](int index) {
		return coords[index];
	}

	void transform(glm::mat4 transform) {
		coords = transform * coords;
	}

	bool operator==(Point3D &other) const {
		return coords.x / coords.w == other.coords.x / coords.w
			   && coords.y / coords.w == other.coords.y / coords.w
			   && coords.z / coords.w == other.coords.z / coords.w;
	}
};

Transform::Transform(const glm::vec4 &position, const glm::vec3 &size) : position(position), size(size) {
	modelMatrix = identityMatrix();
	right = glm::vec3(1, 0, 0);
	up = glm::vec3(0, 1, 0);
	globalUp = glm::vec3(0, 1, 0);
	forward = glm::vec3(0, 0, 1);
}

void Transform::setPosition(glm::vec4 &newPos) {
	position = newPos;
}

glm::vec4 &Transform::getPosition() {
	return position;

}

void Transform::setUp(glm::vec3 &newUp) {
	up = newUp;
}

void Transform::setOrientation(glm::vec3 &newRight, glm::vec3 &newUp, glm::vec3 &newForward) {
	right = newRight;
	up = newUp;
	forward = newForward;
}


const glm::vec3 &Transform::getRight() const {
	return right;
}

const glm::vec3 &Transform::getUp() const {
	return up;
}

const glm::vec3 &Transform::getGlobalUp() const {
	return globalUp;
}

const glm::vec3 &Transform::getForward() const {
	return forward;
}

void Transform::updateForward(glm::vec3 &newForward) {
	glm::vec3 rotationAxis = glm::cross(forward, newForward);
	float rotationAngle = glm::dot(forward, newForward) / (glm::length(forward) * glm::length(newForward));
	rotationAngle = glm::degrees(acos(rotationAngle));

	rotate(rotationAngle, rotationAxis);
}

void Transform::setSize(glm::vec3 &newSize) {
	size = newSize;
}

const glm::vec3 &Transform::getSize() const {
	return size;
}

const glm::mat4 &Transform::getModelMatrix() const {
	return modelMatrix;
}

glm::mat4 Transform::translate3D(glm::vec3 translateVector) {
	glm::mat4 ret(1.0);
	ret[3] = glm::vec4(translateVector, 1.0);

	return ret;
}

glm::mat4 Transform::scale3D(glm::vec3 scaleVector) {
	glm::mat4 ret(1.0);
	for (int i = 0; i < 3; i++) {
		ret[i][i] = scaleVector[i];
	}

	return ret;
}

glm::mat4 Transform::lookAtMatrix(glm::vec3 eye, glm::vec3 center, glm::vec3 viewUp) {
	glm::vec3 c = glm::normalize(center - eye);
	glm::vec3 a = glm::normalize(glm::cross(c, viewUp));
	glm::vec3 b = glm::cross(a, c);

	glm::mat4 ret(
			glm::vec4(a, -glm::dot(a, eye)),
			glm::vec4(b, -glm::dot(b, eye)),
			glm::vec4(-c, glm::dot(c, eye)),
			glm::vec4(0.0, 0.0, 0.0, 1.0)
	);

	return glm::transpose(ret);
}

glm::mat4 Transform::frustum(double l, double r, double b, double t, double n, double f) {
	glm::mat4 ret(0.0);
	ret[0][0] = 2 * n / (r - l);
	ret[1][1] = 2 * n / (t - b);
	ret[2][2] = -(f + n) / (f - n);

	ret[2][0] = (r + l) / (r - l);
	ret[2][1] = (t + b) / (t - b);
	ret[3][2] = -2 * f * n / (f - n);
	ret[2][3] = -1;

	return ret;
}

void Transform::move(glm::vec3 &offset) {
	glm::mat4 translationMatrix = translate3D(offset);
	position = translationMatrix * position;
	modelMatrix = translationMatrix * modelMatrix;

}

void Transform::rotate(float angle, glm::vec3 &axis) {
	glm::mat4 rotationMatrix = glm::rotate(identityMatrix(), glm::radians(angle), axis);
	position = rotationMatrix * position;
	modelMatrix = rotationMatrix * modelMatrix;

	glm::vec4 extRight = glm::vec4(right, 1);
	glm::vec4 extUp = glm::vec4(up, 1);
	glm::vec4 extForward = glm::vec4(forward, 1);

	extRight = rotationMatrix * extRight;
	right = extRight;
	glm::normalize(right);

	extUp = rotationMatrix * extUp;
	up = extUp;
	glm::normalize(up);

	extForward = rotationMatrix * extForward;
	forward = extForward;
	glm::normalize(forward);

}

void Transform::scale(glm::vec3 &scaler) {
	glm::mat4 scalerMatrix = scale3D(scaler);
	position = scalerMatrix * position;
	modelMatrix = scalerMatrix * modelMatrix;

	glm::vec4 extSize = glm::vec4(size, 1);
	extSize = scalerMatrix * extSize;
	for (int i = 0; i < 3; ++i) {
		extSize[i] = abs(extSize[i]);
	}
	size = extSize;
}

void Transform::applyTransform(glm::mat4 &matrix) {
	modelMatrix = matrix * modelMatrix;
	position = matrix * position;

	glm::vec4 extSize = glm::vec4(size, 1);
	extSize = matrix * extSize;
	for (int i = 0; i < 3; ++i) {
		extSize[i] = abs(extSize[i]);
	}
	size = extSize;

	glm::vec4 extRight = glm::vec4(right, 1);
	glm::vec4 extUp = glm::vec4(up, 1);
	glm::vec4 extForward = glm::vec4(forward, 1);

	extRight = matrix * extRight;
	right = extRight;
	glm::normalize(right);

	extUp = matrix * extUp;
	up = extUp;
	glm::normalize(up);

	extForward = matrix * extForward;
	forward = extForward;
	glm::normalize(forward);
}

void Transform::moveCursorX(double dx) {
	rotate((dx / mWidth) * 200, globalUp);
}

void Transform::moveCursorY(double dy) {
	if (glm::dot(up, globalUp) > 0 || (forward[1] * dy > 0)) {
		rotate((dy / mWidth) * -200, right);
	}
}

Camera::Camera(const glm::vec4 &position, const glm::vec3 &size) : Transform(position, size) {
	perspectiveMatrix = identityMatrix();

	center = glm::vec3(0.0);
	glm::vec3 fwd = glm::normalize(glm::vec3(getPosition()) - center);
	auto upProj = glm::normalize(glm::proj(getGlobalUp(), fwd));
	glm::vec3 actualUp = getGlobalUp() - upProj;

	glm::vec3 rght = glm::normalize(glm::cross(fwd, actualUp));

	setOrientation(rght, actualUp, fwd);
}

void Camera::calculatePerspectiveMatrix() {
	perspectiveMatrix = frustum(-0.5, 0.5, -0.5, 0.5, 1.0, 100.0);
}

const glm::mat4 &Camera::getViewMatrix() {
	glm::mat4 mat = lookAtMatrix(glm::vec3(getPosition()), center, getUp());
}

glm::mat4 &Camera::getPerspectiveMatrix() {
	calculatePerspectiveMatrix();
	return perspectiveMatrix;
}

void Camera::move(glm::vec3 &offset) {
	Transform::move(offset);
	glm::mat4 translationMatrix = glm::translate(identityMatrix(), offset);
	center = translationMatrix * glm::vec4(center, 1.0);
}

void Camera::rotate(float angle, glm::vec3 &axis) {
	Transform::rotate(angle, axis);
	glm::mat4 rotationMatrix = glm::rotate(identityMatrix(), glm::radians(angle), axis);
	center = rotationMatrix * glm::vec4(center, 1.0);
}

Light::Light(const glm::vec4 &position, const glm::vec3 &size, aiColor3D Ia, aiColor3D Id, aiColor3D Ir)
		: Transform(position, size), Ia(Ia), Id(Id), Ir(Ir) {}

const aiColor3D &Light::getIa() const {
	return Ia;
}

const aiColor3D &Light::getId() const {
	return Id;
}

const aiColor3D &Light::getIr() const {
	return Ir;
}

const aiColor3D &Material::getAmbient() const {
	if (materialsPresent) {
		return Ka;
	}
	return dummy;
}

void Material::setAmbient(const aiColor3D &ka) {
	Ka = ka;
	materialsPresent = true;
}

const aiColor3D &Material::getDiffuse() const {
	if (materialsPresent) {
		return Kd;
	}
	return dummy;
}

void Material::setDiffuse(const aiColor3D &kd) {
	Kd = kd;
	materialsPresent = true;
}

const aiColor3D &Material::getSpecular() const {
	if (materialsPresent) {
		return Kr;
	}
	return dummy;
}

void Material::setSpecular(const aiColor3D &kr) {
	Kr = kr;
	materialsPresent = true;
}

Material::Material(const aiColor3D &ka, const aiColor3D &kd, const aiColor3D &kr) : Ka(ka), Kd(kd), Kr(kr) {}

Material::Material() {}

class Mesh {
	vector<Point3D> vertices;
	vector<unsigned int> indices;
	vector<glm::vec3> normals;
	vector<Point3D> boundingBox;
	GLuint VAO;
	GLuint VBO_indices;
	GLuint VBO_normals;
	GLuint EBO;

public:

	Mesh(const vector<Point3D> &vertices, const vector<unsigned int> &indices, const vector<glm::vec3> &normals)
			: vertices(vertices), indices(indices), normals(normals) {
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO_indices);
		glGenBuffers(1, &VBO_normals);
		glGenBuffers(1, &EBO);

		fillVAO();
	}

	const vector<Point3D> &getVertices() const {
		return vertices;
	}

	void setVertices(const vector<Point3D> &newVertices) {
		for (const Point3D &vertex: newVertices) {
			vertices.push_back(vertex);
		}
	}

	const vector<unsigned int> &getIndices() const {
		return indices;
	}

	void setIndices(const vector<unsigned int> &newIndices) {
		indices = newIndices;
	}

	GLuint getVao() const {
		return VAO;
	}

	GLuint getVbo() const {
		return VBO_indices;
	}

	GLuint getEbo() const {
		return EBO;
	}

	pair<glm::vec3, glm::vec3> getBoundingBox() {
		glm::vec3 max = glm::vec3(NAN);
		glm::vec3 min = glm::vec3(NAN);

		for (Point3D vertex: vertices) {
			for (int i = 0; i < 3; i++) {
				if (vertex[i] < min[i] || isnan(min[i])) {
					min[i] = vertex[i];
				}

				if (vertex[i] > max[i] || isnan(max[i])) {
					max[i] = vertex[i];
				}
			}
		}
		// prve koordinate su najvece, druge su najmanje
		return make_pair(max, min);
	}

	void applyTransform(glm::mat4 transform) {
		for (Point3D &vertex: vertices) {
			vertex.transform(transform);
		}
		fillVAO();
	}

	void fillVAO() {
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO_indices);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Point3D), &vertices.front(), GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *) 0);

		glBindBuffer(GL_ARRAY_BUFFER, VBO_normals);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals.front(), GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *) 0);

		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), &indices[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
	}

	void deleteBuffers() {
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO_indices);
		glDeleteBuffers(1, &EBO);
	}
};

Shader *loadShaderNoGeom(char *path, char *naziv) {
	std::string sPath(path);
	std::string pathVert;
	std::string pathFrag;

	//malo je nespretno napravljeno jer ne koristimo biblioteku iz c++17, a treba podrzati i windows i linux

	pathVert.append(path, sPath.find_last_of("\\/") + 1);
	pathFrag.append(path, sPath.find_last_of("\\/") + 1);
	if (pathFrag[pathFrag.size() - 1] == '/') {
		pathVert.append("shaders/");
		pathFrag.append("shaders/");
	} else if (pathFrag[pathFrag.size() - 1] == '\\') {
		pathVert.append("shaders\\");
		pathFrag.append("shaders\\");
	} else {
		std::cerr << "nepoznat format pozicije shadera";
		exit(1);
	}

	pathVert.append(naziv);
	pathVert.append(".vert");
	pathFrag.append(naziv);
	pathFrag.append(".frag");

	return new Shader(pathVert.c_str(), pathFrag.c_str());
}

Shader *loadShader(char *path, char *naziv) {
	std::string sPath(path);
	std::string pathVert;
	std::string pathFrag;
	std::string pathGeom;

	//malo je nespretno napravljeno jer ne koristimo biblioteku iz c++17, a treba podrzati i windows i linux

	pathVert.append(path, sPath.find_last_of("\\/") + 1);
	pathFrag.append(path, sPath.find_last_of("\\/") + 1);
	pathGeom.append(path, sPath.find_last_of("\\/") + 1);
	if (pathFrag[pathFrag.size() - 1] == '/') {
		pathVert.append("shaders/");
		pathFrag.append("shaders/");
		pathGeom.append("shaders/");
	} else if (pathFrag[pathFrag.size() - 1] == '\\') {
		pathVert.append("shaders\\");
		pathFrag.append("shaders\\");
		pathGeom.append("shaders\\");
	} else {
		std::cerr << "nepoznat format pozicije shadera";
		exit(1);
	}

	pathVert.append(naziv);
	pathVert.append(".vert");
	pathFrag.append(naziv);
	pathFrag.append(".frag");
	pathGeom.append(naziv);
	pathGeom.append(".geom");

	return new Shader(pathVert.c_str(), pathFrag.c_str(), pathGeom.c_str());
}

class Object {
	Shader *shader;
	Mesh &mesh;
	Transform &transform;
	Material &material;

public:
	Object(char *argv[], Mesh &mesh, Transform &transform, Material &material) : mesh(mesh), transform(transform),
																				 material(material) {
		initShader(argv);
	}

	void initShader(char *argv[]) {
		shader = loadShader(argv[0], "shader");
	}

	Shader *getShader() {
		return shader;
	}

	Mesh &getMesh() {
		return mesh;
	}

	void setMesh(const Mesh &m) {
		mesh = m;
	}

	Transform &getTransform() const {
		return transform;
	}

	Material &getMaterial() const {
		return material;
	}

	void applyTransform(glm::mat4 transfMatrix) {
//		transform.applyTransform(transfMatrix);

	}

	void deleteBuffers() {
		mesh.deleteBuffers();
	}

	bool compatibleObject(Object &other) {
		return shader == other.shader && &mesh == &other.mesh;
	}
};

class BSpline {
	vector<Point3D> &controlPoints;
	vector<Point3D> curvePoints;
	vector<Point3D> tangents;
	Shader *shader;
	Transform &transform;

	GLuint controlPolygonVAO;
	GLuint controlPolygonVBO;
	GLuint curveVAO;
	GLuint curveVBO;

	const glm::mat4 curveWeights = glm::mat4(-1, 3, -3, 1, 3, -6, 3, 0, -3, 0, 3, 0, 1, 4, 1, 0);
	const glm::mat4 tangentWeights = glm::mat3x4(-1, 3, -3, 1, 2, -4, 2, 0, -1, 0, 1, 0);

public:

	BSpline(char *argv[], vector<Point3D> &controlPoints, Transform &transform) : controlPoints(controlPoints),
																				  transform(transform) {
		glGenVertexArrays(1, &controlPolygonVAO);
		glGenBuffers(1, &controlPolygonVBO);
		glGenBuffers(1, &curveVBO);

		shader = loadShaderNoGeom(argv[0], "curveShader");

		calculateCurvePoints();
		calculateTangents();
		fillVAOs();
	}

	GLuint getControlPolygonVAO() const {
		return controlPolygonVAO;
	}

	GLuint getCurveVAO() const {
		return curveVAO;
	}

	Shader *getShader() const {
		return shader;
	}

	Transform &getTransform() const {
		return transform;
	}

	uint controlPolygonSize() {
		return controlPoints.size();
	}

	uint curveSize() {
		return curvePoints.size();
	}

	Point3D getControlPoint(int t) {
		return controlPoints[t];
	}

	Point3D getCurvePoint(int t) {
		return transform.getModelMatrix() * curvePoints[t].coords;
	}

	Point3D getTangent(int t) {
		return tangents[t];
	}

	void calculateCurvePoints() {
		for (size_t i = 3; i < controlPoints.size(); ++i) {
			glm::vec4 temp = glm::vec4(0.0);
			glm::mat4 nodes = glm::mat4(controlPoints[i - 3].coords, controlPoints[i - 2].coords,
										controlPoints[i - 1].coords, controlPoints[i].coords);

			for (float t = 0; t < 1; t += CURVE_PRECISION) {
				glm::vec4 ts = glm::vec4(pow(t, 3), pow(t, 2), t, 1);
				ts /= 6.0;

				temp = nodes * curveWeights * ts;
				curvePoints.emplace_back(temp);
			}
		}
	}

	void calculateTangents() {
		for (size_t i = 3; i < controlPoints.size(); ++i) {
			glm::vec4 temp = glm::vec4(0.0);

			glm::mat4 nodes = glm::mat4(controlPoints[i - 3].coords, controlPoints[i - 2].coords,
										controlPoints[i - 1].coords, controlPoints[i].coords);
			for (float t = 0; t < 1; t += CURVE_PRECISION) {
				glm::vec4 ts = glm::vec4(pow(t, 2), t, 1, 0);
				ts /= 2.0;

				temp = nodes * tangentWeights * ts;
				tangents.emplace_back(glm::normalize(glm::vec3(temp)));
			}
		}
	}

	void fillVAOs() {
		glBindVertexArray(controlPolygonVAO);

		glBindBuffer(GL_ARRAY_BUFFER, controlPolygonVBO);
		glBufferData(GL_ARRAY_BUFFER, controlPoints.size() * sizeof(Point3D), &controlPoints.front(), GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *) 0);
		glEnableVertexAttribArray(0);


		glBindVertexArray(curveVAO);

		glBindBuffer(GL_ARRAY_BUFFER, curveVBO);
		glBufferData(GL_ARRAY_BUFFER, curvePoints.size() * sizeof(Point3D), &curvePoints.front(), GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *) 0);
		glEnableVertexAttribArray(0);

		glBindVertexArray(0);
	}

	void deleteBuffers() {
		glDeleteVertexArrays(1, &controlPolygonVAO);
		glDeleteBuffers(1, &controlPolygonVBO);
		glDeleteBuffers(1, &curveVBO);
	}
};

class Renderer {
	GLFWwindow *window;
	vector<Object> objects;
	vector<vector<unsigned int>> equivObjects;

	BSpline *curve;

	Camera *camera;
	vector<Light> lights;

public:

	Renderer(Camera *cam, vector<Light> lig) : camera(cam), lights(lig) {
		glfwInit();

		window = glfwCreateWindow(mWidth, mHeight, "lab5", nullptr, nullptr);

		// Check for Valid Context
		if (window == nullptr) {
			fprintf(stderr, "Failed to Create OpenGL Context");
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);

//		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
		glfwSetCursorPos(window, mWidth / 2.0, mHeight / 2.0);

		transform_controls::registerTransform(window, cam);

		gladLoadGL();
		glClearColor(0.1, 0.1, 0.1, 1);

		glfwSetFramebufferSizeCallback(window,
									   framebuffer_size_callback); //funkcija koja se poziva prilikom mijenjanja velicine prozora
	}

	void addObject(Object &object) {
		objects.push_back(object);
		bool grouped = false;
		for (auto group: equivObjects) {
			unsigned int index = group[0];
			if (objects[index].compatibleObject(object)) {
				group.push_back(objects.size() - 1);
				grouped = true;
			}
		}
		if (!grouped) {
			vector<unsigned int> newGroup;
			newGroup.push_back(objects.size() - 1);
			equivObjects.push_back(newGroup);
		}
	}

	void draw(int t) {
		glm::vec4 controlPolygonColor = glm::vec4(1.0, 0.0, 0.0, 0.0);
		glm::vec4 curveColor = glm::vec4(1.0, 1.0, 1.0, 0.0);

		GLint curveModelMatrixUniformLocation = glGetUniformLocation(curve->getShader()->ID, "curveModelMatrix");
		GLint curveViewMatrixUniformLocation = glGetUniformLocation(curve->getShader()->ID, "curveViewMatrix");
		GLint curveProjectionMatrixUniformLocation = glGetUniformLocation(curve->getShader()->ID,
																		  "curveProjectionMatrix");
		GLint curveAspectRatioFixUniformLocation = glGetUniformLocation(curve->getShader()->ID, "curveAspectRatioFix");
		GLint colorUniformLocation = glGetUniformLocation(curve->getShader()->ID, "color");

		glm::mat4 curveModelMatrix, curveViewMatrix, curveProjectionMatrix;

		curveModelMatrix = (curve->getTransform().getModelMatrix());
		curveViewMatrix = (camera->getViewMatrix());
		curveProjectionMatrix = camera->getPerspectiveMatrix();

		glUseProgram(curve->getShader()->ID);

		glUniformMatrix4fv(curveModelMatrixUniformLocation, 1, GL_FALSE, &curveModelMatrix[0][0]);
		glUniformMatrix4fv(curveViewMatrixUniformLocation, 1, GL_FALSE, &curveViewMatrix[0][0]);
		glUniformMatrix4fv(curveProjectionMatrixUniformLocation, 1, GL_FALSE, &curveProjectionMatrix[0][0]);
		glUniformMatrix4fv(curveAspectRatioFixUniformLocation, 1, GL_FALSE, &aspectRatioFix[0][0]);
		glUniform4fv(colorUniformLocation, 1, &controlPolygonColor[0]);


		glBindVertexArray(curve->getControlPolygonVAO());

		glDrawArrays(GL_LINE_STRIP, 0, curve->controlPolygonSize());

		glBindVertexArray(curve->getCurveVAO());
		glUniform4fv(colorUniformLocation, 1, &curveColor[0]);
		glDrawArrays(GL_LINE_STRIP, 0, curve->curveSize());

		glBindVertexArray(0);

		for (auto group: equivObjects) {
			Shader *shader = objects[group[0]].getShader();
			Mesh &mesh = objects[group[0]].getMesh();

			GLint modelMatrixUniformLocation = glGetUniformLocation(shader->ID, "modelMatrix");
			GLint viewMatrixUniformLocation = glGetUniformLocation(shader->ID, "viewMatrix");
			GLint projectionMatrixUniformLocation = glGetUniformLocation(shader->ID, "projectionMatrix");

			GLint aspectRatioFixUniformLocation = glGetUniformLocation(shader->ID, "aspectRatioFix");

			GLint eyeUniformLocation = glGetUniformLocation(shader->ID, "eye");
			GLint lightUniformLocation = glGetUniformLocation(shader->ID, "light");
			GLint kaUniformLocation = glGetUniformLocation(shader->ID, "Ka");
			GLint kdUniformLocation = glGetUniformLocation(shader->ID, "Kd");
			GLint krUniformLocation = glGetUniformLocation(shader->ID, "Kr");
			GLint iaUniformLocation = glGetUniformLocation(shader->ID, "Ia");
			GLint idUniformLocation = glGetUniformLocation(shader->ID, "Id");
			GLint irUniformLocation = glGetUniformLocation(shader->ID, "Ir");

			glm::mat4 modelMatrix, viewMatrix, perspectiveMatrix;
			glm::vec4 globalCameraPos;

			glUseProgram(shader->ID);

			glBindVertexArray(mesh.getVao());

			aiColor3D Ka, Kd, Kr, Ia, Id, Ir;

			Light &primaryLight = lights[0];
			Ia = primaryLight.getIa();
			Id = primaryLight.getId();
			Ir = primaryLight.getIr();

			glUniform4fv(lightUniformLocation, 1, &primaryLight.getPosition()[0]);

			glUniform3f(iaUniformLocation, Ia.r, Ia.g, Ia.b);
			glUniform3f(idUniformLocation, Id.r, Id.g, Id.b);
			glUniform3f(irUniformLocation, Ir.r, Ir.g, Ir.b);

			glUniform4fv(eyeUniformLocation, 1, &camera->getPosition()[0]);


			for (size_t i = 0; i < group.size(); ++i) {
				auto currentTransform = objects[group[i]].getTransform();

				auto offset = glm::xyz(-currentTransform.getPosition());
				currentTransform.move(offset);

				auto newForward = glm::xyz(curve->getTangent(t).coords);
				currentTransform.updateForward(newForward);

				offset = glm::xyz(curve->getCurvePoint(t).coords);
				currentTransform.move(offset);

				bool idk = (currentTransform.getModelMatrix() == objects[group[i]].getTransform().getModelMatrix());

				cout<<"t = "<<t<<", model matrix match = "<<idk<<endl<<endl;

				modelMatrix = (currentTransform.getModelMatrix());
				viewMatrix = (camera->getViewMatrix());
				perspectiveMatrix = camera->getPerspectiveMatrix();

				Material &material = objects[group[i]].getMaterial();
				Ka = material.getAmbient();
				Kd = material.getDiffuse();
				Kr = material.getSpecular();

//				globalCameraPos = perspectiveMatrix * viewMatrix * camera->getPosition();
//				cout<<glm::to_string(modelMatrix); 
				glUniformMatrix4fv(modelMatrixUniformLocation, 1, GL_FALSE, &modelMatrix[0][0]);
				glUniformMatrix4fv(viewMatrixUniformLocation, 1, GL_FALSE, &viewMatrix[0][0]);
				glUniformMatrix4fv(projectionMatrixUniformLocation, 1, GL_FALSE, &perspectiveMatrix[0][0]);
				glUniformMatrix4fv(aspectRatioFixUniformLocation, 1, GL_FALSE, &aspectRatioFix[0][0]);
//				glUniform4fv(eyeUniformLocation, 1, &globalCameraPos[0]);

				glUniform3f(kaUniformLocation, Ka.r, Ka.g, Ka.b);
				glUniform3f(kdUniformLocation, Kd.r, Kd.g, Kd.b);
				glUniform3f(krUniformLocation, Kr.r, Kr.g, Kr.b);

				glDrawElements(GL_TRIANGLES, mesh.getIndices().size(), GL_UNSIGNED_INT, 0);
			}

			glBindVertexArray(0);
		}
	}

	GLFWwindow *getWindow() const {
		return window;
	}

	const vector<Object> &getObjects() const {
		return objects;
	}

	void setCurve(BSpline *curve) {
		Renderer::curve = curve;
	}

	void deleteBuffers() {
		for (Object o: objects) {
			delete o.getShader();
			o.deleteBuffers();
		}

		curve->deleteBuffers();
	}
};

class ImportedObject {
	unsigned int numVertices, numFaces;
	vector<aiVector3D> vertices;
	vector<aiFace> faces;
	aiVector3D *normals;
	Material material;

public:
	ImportedObject(char *argv[], const std::string &name) {
		Assimp::Importer importer;

		std::string path(argv[0]);
		std::string dirPath(path, 0, path.find_last_of("\\/"));
		std::string resPath(dirPath);
		resPath.append("/resources");
		std::string objPath(resPath);
		objPath.append(name);

		const aiScene *scene = importer.ReadFile(objPath.c_str(),
												 aiProcess_CalcTangentSpace |
												 aiProcess_Triangulate |
												 aiProcess_JoinIdenticalVertices |
												 aiProcess_SortByPType);
		if (!scene) {
			std::cerr << importer.GetErrorString();
			exit(1);
		}

		aiMesh *mesh = scene->mMeshes[0];
		numVertices = mesh->mNumVertices;
		numFaces = mesh->mNumFaces;

		for (size_t i = 0; i < numVertices; ++i) {
			vertices.emplace_back(mesh->mVertices[i]);
		}

		for (size_t i = 0; i < numFaces; ++i) {
			faces.emplace_back(mesh->mFaces[i]);
		}

		normals = mesh->mNormals;

		if (normals == nullptr) {
			normals = (aiVector3D *) (malloc(numVertices * sizeof(aiVector3D)));

			// calculate normals
			for (uint i = 0; i < numVertices; i++) {
				glm::vec3 normal(0.0);
				int numAdjFaces = 0;
				for (const auto &face: faces) {
					for (uint j = 0; j < face.mNumIndices; j++) {
						if (i == j) {
							auto indices = face.mIndices;
							numAdjFaces++;

							auto temp = vertices[indices[1]] - vertices[indices[0]];
							glm::vec3 vec1(temp.x, temp.y, temp.z);
							temp = vertices[indices[2]] - vertices[indices[0]];
							glm::vec3 vec2(temp.x, temp.y, temp.z);

							normal += (glm::cross(vec1, vec2));
						}
					}
				}
				normal /= numAdjFaces;
				glm::normalize(normal);

				normals[i] = aiVector3D(normal.x, normal.y, normal.z);
			}
		}

		if (scene->HasMaterials() && scene->mNumMaterials > 1) {
			aiMaterial *importedMat = scene->mMaterials[1];

			aiColor3D ambient, diffuse, specular;
			importedMat->Get(AI_MATKEY_COLOR_AMBIENT, ambient);
			importedMat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
			importedMat->Get(AI_MATKEY_COLOR_SPECULAR, specular);

			material.setAmbient(ambient);
			material.setDiffuse(diffuse);
			material.setSpecular(specular);
		}
	}

	unsigned int getNumVertices() const {
		return numVertices;
	}

	unsigned int getNumFaces() const {
		return numFaces;
	}

	const vector<aiVector3D> &getVertices() const {
		return vertices;
	}

	const vector<aiFace> &getFaces() const {
		return faces;
	}

	const Material &getMaterial() const {
		return material;
	}

	aiVector3D *getNormals() const {
		return normals;
	}
};


int main(int argc, char *argv[]) {
	bool play = true;
	float t = 0;

	Camera camera(glm::vec4(-6, 10, 10, 1), glm::vec3(0, 0, 0));
	glm::vec3 off = glm::vec3(4.0, 0.0, 2.0);
	camera.move(off);

	aiColor3D a1(0.5, 0.5, 0.5);
	aiColor3D d1(0.8, 0.8, 0.8);
	aiColor3D r1(0.5, 0.5, 0.5);

	vector<Light> lights;
	lights.emplace_back(glm::vec4(3, 4, 1, 1), glm::vec3(0, 0, 0), a1, d1, r1);

	Renderer renderer(&camera, lights);

	ImportedObject importedCurve(argv, "/curves/curve1.obj");

	unsigned int numControlPoints = importedCurve.getNumVertices();
	auto &importedControlPoints = importedCurve.getVertices();

	vector<Point3D> controlPoints;
	for (size_t i = 0; i < numControlPoints; ++i) {
		auto temp = importedControlPoints[i];
		controlPoints.emplace_back(temp.x, temp.y, temp.z);
	}

	glm::vec3 max = glm::vec3(NAN);
	glm::vec3 min = glm::vec3(NAN);

	for (Point3D vertex: controlPoints) {
		for (int i = 0; i < 3; i++) {
			if (vertex[i] < min[i] || isnan(min[i])) {
				min[i] = vertex[i];
			}

			if (vertex[i] > max[i] || isnan(max[i])) {
				max[i] = vertex[i];
			}
		}
	}
	// prve koordinate su najvece, druge su najmanje
	pair<glm::vec3, glm::vec3> controlPolygonSpan = make_pair(max, min);


	glm::vec3 controlPolygonSize;
	glm::vec4 controlPolygonPosition;

	for (int i = 0; i < 3; ++i) {
		controlPolygonSize[i] = controlPolygonSpan.first[i] - controlPolygonSpan.second[i];
		controlPolygonPosition[i] = (controlPolygonSpan.first[i] + controlPolygonSpan.second[i]) / 2;
	}

	controlPolygonPosition[3] = 1;

	Transform controlPolygonTransform(controlPolygonPosition, controlPolygonSize);

	BSpline curve(argv, controlPoints, controlPolygonTransform);

	glm::vec3 CPOffset = glm::vec3(-controlPolygonPosition);
//	controlPolygonTransform.move(CPOffset);

	float curveSpan = fmax(controlPolygonSize.x, fmax(controlPolygonSize.y, controlPolygonSize.z));
	float scl = 2.0 / curveSpan;

	glm::vec3 curveScaler = glm::vec3(scl);
//	controlPolygonTransform.scale(curveScaler);

	renderer.setCurve(&curve);

	// IMPORTED OBJECT

//	ImportedObject importedObject(argv, "/747/aircraft747.obj");
	  ImportedObject importedObject(argv, "/arrow/arrow.obj");

	unsigned int numVertices = importedObject.getNumVertices();
	auto &importedVertices = importedObject.getVertices();
	unsigned int numFaces = importedObject.getNumFaces();
	auto &importedFaces = importedObject.getFaces();

	Material importedMaterial = importedObject.getMaterial();
//	const aiMesh *importedMesh = importedObject.getMesh();

	vector<Point3D> vertices;
	for (size_t i = 0; i < numVertices; ++i) {
		auto temp = importedVertices[i];
		vertices.emplace_back(temp.x, temp.y, temp.z);
	}

	vector<unsigned int> indices;
	vector<glm::vec3> normals;
	auto importedNormals = importedObject.getNormals();

	for (size_t i = 0; i < numFaces; ++i) {
		auto faceIndices = importedFaces[i].mIndices;
		for (size_t j = 0; j < importedFaces[i].mNumIndices; ++j) {
			indices.push_back(faceIndices[j]);
			if (importedNormals != nullptr) {
				aiVector3D temp = importedNormals[importedFaces[i].mIndices[j]];
				normals.emplace_back(temp.x, temp.y, temp.z);
			}
		}
	}

	Mesh mesh(vertices, indices, normals);

	pair<glm::vec3, glm::vec3> span = mesh.getBoundingBox();

	glm::vec3 size;
	glm::vec4 position = glm::vec4(0.0, 0.0, 0.0, 1.0);

	for (int i = 0; i < 3; ++i) {
		size[i] = span.first[i] - span.second[i];
//		position[i] = (span.first[i] + span.second[i]) / 2;
	}

//	position[3] = 1;

	// IMPORTED OBJECT OVER

	Transform transform(position, size);
//	Transform transform2(position, size);

	// TODO POCETNA ORIJENTACIJA !
	glm::vec4 initialOrientation = curve.getTangent(0).coords;
	glm::vec3 swizzled = glm::xyz(initialOrientation);

	Object object(argv, mesh, transform, importedMaterial);
//	Object object2(argv, mesh, transform2, importedMaterial);

	glm::vec3 offset = glm::vec3(-position);
//	transform.move(offset);
	transform.updateForward(swizzled);
	auto up = glm::vec3(0.0, 1.0, 0.0);
//	transform.rotate(90, up);
	offset = (curve.getTransform().getModelMatrix() * curve.getCurvePoint(0).coords);
	transform.move(offset);
//	transform2.move(offset);

	float maxSpan = fmax(size.x, fmax(size.y, size.z));
	float sc = 2.0 / maxSpan;

	glm::vec3 scaler = glm::vec3(sc);
//	transform.scale(scaler);
//	transform2.scale(scaler);

	glm::vec3 axis(1.0, 1.0, 1.0);
//	transform2.rotate(30, axis);
	glm::vec3 move2(-2.0, 0.0, 2.0);
//	transform2.move(move2);
	glm::vec3 move1(0.0, 0.0, -1.0);
//	transform.move(move1);

//	transform.translate3D(move2);

//	glm::vec3 rotato = glm::vec3(1, 0, 0);
//	float angulus = 90;
//
//	glm::mat4 rotatoMatro = transform.rotate(angulus, rotato);


//	object.applyTransform(translationMatrix);
//	object.applyTransform(scalerMatrix);
//	object.applyTransform(rotatoMatro);

	renderer.addObject(object);
//	renderer.addObject(object2);

	auto window = renderer.getWindow();

	glEnable(GL_DEPTH_TEST);

	while (glfwWindowShouldClose(window) == false) {

		glClear(GL_COLOR_BUFFER_BIT);
		glClear(GL_DEPTH_BUFFER_BIT);

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);

		if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
			play = false;
		}

		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			play = true;
		}

		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
			t = 0;
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			ANIMATION_SPEED += 2.0;
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			ANIMATION_SPEED -= 2.0;
		}

		cout<<"play = "<<play<<endl;

		renderer.draw(t);

		glfwSwapBuffers(window);
		glfwPollEvents();

		if (play) {
			t += ANIMATION_SPEED;
			if(t > curve.curveSize()) {
				t -= curve.curveSize();
			}

			if(t < 0) {
				t = curve.curveSize();
			}
		}
	}

	renderer.deleteBuffers();

	glfwTerminate();

	return 0;
}