#version 330 core
layout (location = 0) in vec4 aPos;

uniform mat4 curveModelMatrix;
uniform mat4 curveViewMatrix;
uniform mat4 curveProjectionMatrix;
uniform mat4 curveAspectRatioFix;

void main()
{
    gl_Position = curveAspectRatioFix * curveProjectionMatrix * curveViewMatrix * curveModelMatrix * aPos;
}