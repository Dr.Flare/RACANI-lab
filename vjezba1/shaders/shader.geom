#version 330 core
layout (triangles) in;
in vec3 vertNormal[];
out vec3 col;
layout (triangle_strip, max_vertices = 3) out;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 aspectRatioFix;
uniform vec4 eye;

uniform vec4 light;
uniform vec3 Ia;
uniform vec3 Id;
uniform vec3 Ir;

uniform vec3 Ka;
uniform vec3 Kd;
uniform vec3 Kr;

void main() {
    vec4 a4 = gl_in[0].gl_Position;
    vec4 b4 = gl_in[1].gl_Position;
    vec4 c4 = gl_in[2].gl_Position;

    vec3 a = vec3(a4[0]/a4[3], a4[1]/a4[3], a4[2]/a4[3]);
    vec3 b = vec3(b4[0]/b4[3], b4[1]/b4[3], b4[2]/b4[3]);
    vec3 c = vec3(c4[0]/c4[3], c4[1]/c4[3], c4[2]/c4[3]);

    vec3 eye3 = vec3(eye[0]/eye[3], eye[1]/eye[3], eye[2]/eye[3]);

    vec3 v1 = b - a;
    vec3 v2 = c - a;

    vec3 normal = normalize(cross(v1, v2));

    vec3 center = (a + b + c) / 3.0;

    vec3 camera_vector = normalize(eye3 - center);

    float dot_prod = dot(camera_vector, normal);

//    float camera_cos = dot_prod / (length(camera_vector) * length(normal)) ;

    vec3 light3 = vec3(light[0]/light[3], light[1]/light[3], light[2]/light[3]);
    vec3 light_vector = normalize(center - light3);

    float diff_prod = dot(light_vector, normal);

    vec3 reverse_light = - light_vector;

    vec3 refl = reverse_light + 2 * diff_prod * normalize(normal);

    float refl_prod = dot(camera_vector, refl);

    refl_prod = pow(refl_prod, 10);

    float ambient_r = Ia.r * Ka.r;
    float ambient_g = Ia.g * Ka.g;
    float ambient_b = Ia.b * Ka.b;

    float diffuse_r = Id.r * Kd.r * diff_prod;
    float diffuse_g = Id.g * Kd.g * diff_prod;
    float diffuse_b = Id.b * Kd.b * diff_prod;

    float specular_r = max(Ir.r * Kr.r * refl_prod, 0);
    float specular_g = max(Ir.g * Kr.g * refl_prod, 0);
    float specular_b = max(Ir.b * Kr.b * refl_prod, 0);

    float Ir = ambient_r + diffuse_r + specular_r;
    float Ig = ambient_g + diffuse_g + specular_g;
    float Ib = ambient_b + diffuse_b + specular_b;

    col = vec3(Ir, Ig, Ib);

    if (dot_prod <= 0) {
        gl_Position = aspectRatioFix * projectionMatrix * viewMatrix * gl_in[0].gl_Position;
        EmitVertex();
        gl_Position = aspectRatioFix * projectionMatrix * viewMatrix * gl_in[1].gl_Position;
        EmitVertex();
        gl_Position = aspectRatioFix * projectionMatrix * viewMatrix * gl_in[2].gl_Position;
        EmitVertex();

        EndPrimitive();
    }

//    gl_Position = aspectRatioFix * projectionMatrix * viewMatrix * eye;
//    EmitVertex();
//    gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
//    EmitVertex();
//    EndPrimitive();
}