#version 330 core
layout (location = 0) in vec4 aPos;
layout (location = 1) in vec3 vertexNormal;

out vec3 vertNormal;

uniform mat4 modelMatrix;

void main()
{
    vertNormal = vertexNormal;
    gl_Position = modelMatrix * aPos;
}