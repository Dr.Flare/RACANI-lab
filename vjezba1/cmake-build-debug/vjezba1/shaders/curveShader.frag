#version 330 core
out vec4 aCol;

uniform vec4 color;

void main()
{
    aCol = color;
}