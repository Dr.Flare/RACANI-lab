#pragma once
#include "main.h"
#include <GLFW/glfw3.h>

namespace transform_controls{
	void registerTransform (GLFWwindow *w, Transform *t);
	void registerCallback();
}