#pragma once
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define M_WIDTH 1280
#define M_HEIGHT 800

class Transform {
	glm::vec4 position;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 forward;

	glm::vec3 size;

	glm::vec3 globalUp;


	glm::mat4 modelMatrix;
public:
	Transform(const glm::vec4 &position, const glm::vec3 &size);

	void setPosition(glm::vec4 &newPos);

	glm::vec4 &getPosition();

	void setUp(glm::vec3 &newUp);

	void setOrientation(glm::vec3 &newRight, glm::vec3 &newUp, glm::vec3 &newForward);

	void updateForward(glm::vec3 &newForward);

	const glm::vec3 &getRight() const;

	const glm::vec3 &getUp() const;

	const glm::vec3 &getGlobalUp() const;

	const glm::vec3 &getForward() const;

	void setSize(glm::vec3 &newSize);

	const glm::vec3 &getSize() const;

	const glm::mat4 &getModelMatrix() const;

	static glm::mat4 translate3D(glm::vec3 translateVector);

	static glm::mat4 scale3D(glm::vec3 scaleVector);

	static glm::mat4 lookAtMatrix(glm::vec3 eye, glm::vec3 center, glm::vec3 viewUp);

	static glm::mat4 frustum(double l, double r, double b, double t, double n, double f);

	virtual void move(glm::vec3 &offset);

	virtual void rotate(float angle, glm::vec3 &axis);

	virtual void scale(glm::vec3 &scaler);

	virtual void applyTransform(glm::mat4 &matrix);

	void moveCursorX(double dx);

	void moveCursorY(double dy);
};

class Camera : public Transform {
	glm::mat4 perspectiveMatrix;
	glm::vec3 center;
public:
	Camera(const glm::vec4 &position, const glm::vec3 &size);

	void calculatePerspectiveMatrix();

	const glm::mat4 &getViewMatrix();

	glm::mat4 &getPerspectiveMatrix();

	void move(glm::vec3 &offset);

	void rotate(float angle, glm::vec3 &axis);

	glm::vec3 getCenter();
};


class Light : public Transform {
	aiColor3D Ia, Id, Ir;
public:

	Light(const glm::vec4 &position, const glm::vec3 &size, aiColor3D Ia, aiColor3D Id, aiColor3D Ir);

	const aiColor3D &getIa() const;

	const aiColor3D &getId() const;

	const aiColor3D &getIr() const;
};

class Material {
	bool materialsPresent = false;
	aiColor3D dummy = aiColor3D(0, 0, 0);
	aiColor3D Ka, Kd, Kr;

public:
	Material();

	Material(const aiColor3D &ka, const aiColor3D &kd, const aiColor3D &kr);

	const aiColor3D &getAmbient() const;

	void setAmbient(const aiColor3D &ka);

	const aiColor3D &getDiffuse() const;

	void setDiffuse(const aiColor3D &kd);

	const aiColor3D &getSpecular() const;

	void setSpecular(const aiColor3D &kr);

	bool isPresent() const {
		return materialsPresent;
	}
};