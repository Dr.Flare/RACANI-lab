using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CameraFollowsCar : MonoBehaviour
{
    public GameObject car;
    public GameObject ball;
    public float distance = 6;
    public float elevation = 3;
    public float angle = 10;
    public bool ballCam = false;

    private Camera cam;
    
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (ballCam)
        {
            cam.transform.position = car.transform.position 
                                     - (ball.transform.position - cam.transform.position).normalized * distance 
                                     + elevation * Vector3.up;
            
            cam.transform.LookAt(ball.transform);
        }
        else
        {
            cam.transform.position = car.transform.position 
                                     - car.transform.forward * distance 
                                     + elevation * Vector3.up;
            
            cam.transform.forward = car.transform.forward;
            cam.transform.Rotate(Vector3.right, angle);
        }
    }

    public void ToggleBallCam()
    {
        ballCam = !ballCam;
    }
}
