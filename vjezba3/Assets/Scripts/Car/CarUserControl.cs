using System;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    // [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {
        public CarController m_Car; // the car controller we want to use

        private uint ballCamCooldown = 5;
        private uint ballCamTimer;
        private bool ballCamHold = false;

        private void Awake()
        {
            ballCamTimer = ballCamCooldown;

            // get the car controller
            m_Car = GetComponent<CarController>();
        }


        private void FixedUpdate()
        {
            if (ballCamTimer > 0)
            {
                ballCamTimer--;
            }

            // pass the input to the car!
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            float accel = CrossPlatformInputManager.GetAxis("Accelerate");
            float brake = CrossPlatformInputManager.GetAxis("Brake");
            float pitch = CrossPlatformInputManager.GetAxis("Vertical");
            float roll = CrossPlatformInputManager.GetAxis("Roll");
#if !MOBILE_INPUT
            float handbrakeFloat = CrossPlatformInputManager.GetAxis("Handbrake");
            float boostFloat = CrossPlatformInputManager.GetAxis("Boost");
            float jumpFloat = CrossPlatformInputManager.GetAxis("Jump");
            float ballCamFloat = CrossPlatformInputManager.GetAxis("BallCam");

            bool boost = boostFloat > 0.5f;
            bool handbrake = handbrakeFloat < -0.5f;
            bool jump = jumpFloat > 0.5f;
            Debug.Log(handbrake);
            if (ballCamTimer == 0)
            {
                if (ballCamFloat > 0.5f)
                {
                    if (!ballCamHold)
                    {
                        FindObjectOfType<CameraFollowsCar>().ToggleBallCam();
                        ballCamTimer = ballCamCooldown;
                        ballCamHold = true;
                    }
                }
                else
                {
                    ballCamHold = false;
                }
            }

            m_Car.Move(horizontal, pitch, roll, accel, brake, handbrake, boost, jump);
#else
            m_Car.Move(h, v, v, 0f);
#endif
        }
    }
}