using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Vehicles.Car;

public class BallScript : MonoBehaviour
{
    [SerializeField] public TMP_Text northScoreText;
    [SerializeField] public TMP_Text southScoreText;

    private uint northScore = 0;
    private uint southScore = 0;

    private uint explosionTime = 30;
    private uint explosionTimer;
    private bool explosionStarted = false;

    private void Start()
    {
        GetComponent<ParticleSystemII.ParticleSystem>().enabled = false;
    }

    private void Update()
    {
        if (explosionStarted)
        {
            if (explosionTimer <= 0)
            {
                explosionStarted = false;
                GetComponent<ParticleSystemII.ParticleSystem>().enabled = false;
                transform.position = new Vector3(0f, 2.1f, 0f);
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                var car = FindObjectOfType<CarController>();
                car.transform.position = new Vector3(0, 0.1f, -30);
                car.transform.rotation = Quaternion.identity;
                car.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            }
            else
            {
                explosionTimer--;
            }
        }
    }

    private void RespawnEverything()
    {
        GetComponent<ParticleSystemII.ParticleSystem>().enabled = true;
        explosionTimer = explosionTime;
        explosionStarted = true;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("North Goal"))
        {
            northScore++;
            northScoreText.text = northScore.ToString();
            RespawnEverything();
        }

        if (collider.gameObject.CompareTag("South Goal"))
        {
            southScore++;
            southScoreText.text = southScore.ToString();
            RespawnEverything();
        }
    }
}