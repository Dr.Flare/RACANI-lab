﻿using System;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ParticleSystemII
{
    public class ParticleSystem : MonoBehaviour
    {
        private const uint maxParticles = 10000;
        private uint currentParticles = 0;

        private bool enabled = true;
        
        public bool pane = true;
        
        public float particleLifetime = 1;
        public uint particleLifetimeVariance = 100;

        public float spawnInterval = 2f;
        public uint spawnIntervalVariance = 100;

        public GameObject particleObject;

        public Vector3 particlePosition = Vector3.zero;
        public Vector3 particlePositionVariance = Vector3.zero;

        public Vector3 particleVelocity = new(0f, 0.0f, 0f);
        public Vector3 particleVelocityVariance = Vector3.zero;

        public Vector3 particleAccel = new(0f, 0.0f, 0f);
        public Vector3 particleAccelVariance = Vector3.zero;

        private float FPS;
        private uint spawnCounter = 0;
        private float nextSpawnTimeVariance = 0;

        private void Start()
        {
            FPS = 1f / Time.deltaTime;
            Random.InitState(Guid.NewGuid().GetHashCode());
        }

        private void Update()
        {
            if (Math.Abs(spawnInterval) < 0.000001 || !enabled)
            {
                return;
            }
            
            spawnCounter++;
            
            float particleSpawnNo = (spawnCounter + nextSpawnTimeVariance) / (FPS * spawnInterval);
            
            while (currentParticles < maxParticles && particleSpawnNo >= 1)
            {
                spawnCounter = 0;
                particleSpawnNo--;
                float x, y, z; // temp


                x = Random.Range(particlePosition.x - (particlePositionVariance.x / 2),
                    particlePosition.x + (particlePositionVariance.x / 2));
                y = Random.Range(particlePosition.y - (particlePositionVariance.y / 2),
                    particlePosition.y + (particlePositionVariance.y / 2));
                z = Random.Range(particlePosition.z - (particlePositionVariance.z / 2),
                    particlePosition.z + (particlePositionVariance.z / 2));

                var newPos = new Vector3(x, y, z);

                var obj = Instantiate(particleObject, transform.TransformPoint(newPos), transform.rotation);
                Particle particleScript = obj.GetComponent<Particle>();

                particleScript.setParticleSystem(this);
                
                particleScript.position = newPos;
                particleScript.pane = pane;
                // particleScript.GetComponent<Renderer>().material.color = Color.blue;

                // print("generirana vrijednost inicijalne pozicije: " + particleScript.position);

                x = Random.Range(particleVelocity.x - (particleVelocityVariance.x / 2),
                    particleVelocity.x + (particleVelocityVariance.x / 2));
                y = Random.Range(particleVelocity.y - (particleVelocityVariance.y / 2),
                    particleVelocity.y + (particleVelocityVariance.y / 2));
                z = Random.Range(particleVelocity.z - (particleVelocityVariance.z / 2),
                    particleVelocity.z + (particleVelocityVariance.z / 2));

                particleScript.velocity = transform.TransformVector(new Vector3(x, y, z));

                x = Random.Range(particleAccel.x - (particleAccelVariance.x / 2),
                    particleAccel.x + (particleAccelVariance.x / 2));
                y = Random.Range(particleAccel.y - (particleAccelVariance.y / 2),
                    particleAccel.y + (particleAccelVariance.y / 2));
                z = Random.Range(particleAccel.z - (particleAccelVariance.z / 2),
                    particleAccel.z + (particleAccelVariance.z / 2));

                particleScript.acceleration = transform.TransformVector(new Vector3(x, y, z));

                particleScript.lifetime = Random.Range(particleLifetime - (particleLifetimeVariance / 2),
                    particleLifetime + (particleLifetimeVariance / 2));

                // print("lifetime = " + particleScript.lifetime + " iz min " +
                      // (particleLifetime - (particleLifetimeVariance / 2)) + " i max " +
                      // (particleLifetime + (particleLifetimeVariance / 2)));


                currentParticles++;

                nextSpawnTimeVariance = Random.Range(spawnInterval - (spawnIntervalVariance / 2f),
                    spawnInterval + (spawnIntervalVariance / 2f));
            }
        }

        public void setEnabled(bool _enabled)
        {
            enabled = _enabled;
        }

        public void particleDied()
        {
            currentParticles--;
        }
    }
}